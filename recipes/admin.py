from django.contrib import admin
from recipes.models import Recipe, Measure, FoodItems, Ingredients, Step, Tag


admin.site.register(Recipe)
admin.site.register(Measure)
admin.site.register(FoodItems)
admin.site.register(Ingredients)
admin.site.register(Step)
admin.site.register(Tag)

# Register your models here.
