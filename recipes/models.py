from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125, null=True)
    author = models.CharField(max_length=100, null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name + " by" + self.author


class Measure(models.Model):
    pass

    name = models.CharField(max_length=30, unique=True, null=True)
    abbreviation = models.CharField(max_length=10, unique=True, null=True)

    def __str__(self):
        return self.name + " by " + self.abbreviation


class FoodItems(models.Model):
    pass

    name = models.CharField(max_length=100, unique=True, null=True)

    def __str__(self):
        return self.name


class Ingredients(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey(
        Recipe, related_name="ingredients", on_delete=models.CASCADE
    )
    measure = models.ForeignKey(Measure, on_delete=models.PROTECT)
    food = models.ForeignKey(FoodItems, on_delete=models.PROTECT)

    def __str__(self):
        return self.amount


class Step(models.Model):
    step = models.ForeignKey(
        Recipe, related_name="steps", on_delete=models.CASCADE
    )
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300, null=True)

    def __str__(self):
        return self.step


# That function, the __str__ method, is what gets called by Python
# when it wants to turn it into a string. You can use any of the
# properties that you defined in the __str__ method.

# The self in there is the specific instance of the Recipe. All Recipe
# instances will share that behavior. Each Recipe instance will have
# its own name, author, etc. That means, self.name will be the name of
# that particular recipe.
class Tag(models.Model):
    name = models.CharField(max_length=20, unique=True, null=True)
    recipe = models.ManyToManyField(Recipe, related_name="tags")

    def __str__(self):
        return self.tag
